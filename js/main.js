document.getElementById("btnThemNV").onclick = themNguoiDung
document.getElementById("btnCapNhat").onclick = capNhatNguoiDung
document.getElementById("btnTimNV").onclick = timKiemLoaiNV
// global 
var dsnv = []
if (localStorage.getItem("DSNV") != null) {
    dataLocalStorage = JSON.parse(localStorage.getItem("DSNV"))
    dsnv = dataLocalStorage.map(function (item) {
        return new nhanVien(
            item.tknv,
            item.name,
            item.email,
            item.password,
            item.datepicker,
            item.luongCB,
            item.chucvu,
            item.giolam,
        )
    })
    render(dsnv)

    // render(dsnv)
}

function kiemTraGiaTriNhapVao(nv) {
    var isValid = true;
    isValid = (isNull(nv.tknv, "tbTKNV") && kiemTraSo(nv.tknv, "tbTKNV") && kiemTraSoKyTu(nv.tknv, "tbTKNV", 4, 6)) &
        (isNull(nv.name, "tbTen") && kiemTraChu(nv.name, "tbTen")) &
        (isNull(nv.email, "tbEmail") && kiemTraEmail(nv.email, "tbEmail")) &
        (isNull(nv.password, "tbMatKhau") && kiemTraPass(nv.password, "tbMatKhau")) &
        (isNull(nv.datepicker, "tbNgay") && kiemTraNgay(nv.datepicker, "tbNgay")) &
        (isNull(nv.luongCB, "tbLuongCB") && kiemTraLuongCB(nv.luongCB, "tbLuongCB")) &
        (kiemTraChucVu(nv.chucvu, "tbChucVu")) &
        (isNull(nv.giolam, "tbGiolam") && kiemTraSoGioLam(nv.giolam, "tbGiolam"))
    return isValid
}
// fuction them nhan vien
function themNguoiDung() {
    var nv = layThongTinNguoiDung();

    if (kiemTraGiaTriNhapVao(nv)) {
        dsnv.push(nv)
        localStorage.setItem("DSNV", JSON.stringify(dsnv))
        render(dsnv)
    }

}

//fuction xoa nhan vien
function xoaNguoiDung(tknv) {
    var viTri = timViTri(tknv, dsnv)
    if (viTri != -1) {
        dsnv.splice(viTri, 1)
    }
    localStorage.setItem("DSNV", JSON.stringify(dsnv))
    render(dsnv)
}

//fuction sua nhan vien
function suaNguoiDung(tknv) {
    var viTri = timViTri(tknv, dsnv)
    xuatThongTinNguoiDung(dsnv[viTri])
}

//fuction cập nhật nhân viên
function capNhatNguoiDung() {
    var nv = layThongTinNguoiDung()
    if (kiemTraGiaTriNhapVao(nv)) {
        var viTri = timViTri(nv.tknv, dsnv)
        dsnv[viTri] = nv
        localStorage.setItem("DSNV", JSON.stringify(dsnv))
        render(dsnv)
    }
}
//fuction tìm kiếm loại nhân viên
function timKiemLoaiNV(){
    var loaiNV = document.getElementById("searchName").value; 
    var dsnvTimKiem
    if(loaiNV == "Xuất sắc"){
        dsnvTimKiem = dsnv.filter(function(item){
            return item.xepLoai() == "Xuất sắc"
        })
    }else if(loaiNV == "Giỏi"){
        dsnvTimKiem = dsnv.filter(function(item){
            return item.xepLoai() == "Giỏi"
        })
    }else if(loaiNV == "Khá"){
        dsnvTimKiem = dsnv.filter(function(item){
            return item.xepLoai() == "Khá"
        })
    }else if(loaiNV == "Trung bình"){
        dsnvTimKiem = dsnv.filter(function(item){
            return item.xepLoai() == "Trung bình"
        })
    }else{
        alert("Nhập loại nhân viên cần tìm kiềm")
        render(dsnv)
        return
    }
    render(dsnvTimKiem)
}
