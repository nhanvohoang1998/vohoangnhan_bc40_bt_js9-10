function layThongTinNguoiDung() {
    var _tknv = document.getElementById("tknv").value;
    var _name = document.getElementById("name").value;
    var _email = document.getElementById("email").value;
    var _password = document.getElementById("password").value;
    var _datepicker = document.getElementById("datepicker").value;
    var _luongCB = document.getElementById("luongCB").value;
    var _chucvu = document.getElementById("chucvu").value;
    var _gioLam = document.getElementById("gioLam").value;
    var nv = new nhanVien(
        _tknv,
        _name,
        _email,
        _password,
        _datepicker,
        _luongCB,
        _chucvu,
        _gioLam,
    )
    return nv;
}

function render(dsnv) {
    var tableDanhSach = document.getElementById("tableDanhSach")
    var content = ""
    for (var i = 0; i < dsnv.length; i++) {
        content += `<tr>
                    <td>${dsnv[i].tknv}</td>
                    <td>${dsnv[i].name}</td>
                    <td>${dsnv[i].email}</td>
                    <td>${dsnv[i].datepicker}</td>
                    <td>${dsnv[i].chucvu}</td>
                    <td>${dsnv[i].tongLuong()}</td>
                    <td>${dsnv[i].xepLoai()}</td>
                    <td>
                        <button class="btn btn-danger" onclick="xoaNguoiDung(${dsnv[i].tknv})">Xóa</button>
                        <button class="btn btn-warning" data-toggle="modal"
                        data-target="#myModal" onclick="suaNguoiDung(${dsnv[i].tknv})">Sửa</button>
                    </td>
                    </tr>
                    `
    }
    tableDanhSach.innerHTML = content
}

function timViTri(tknv, dsnv) {
    var viTri = -1;
    for (var i = 0; i < dsnv.length; i++) {
        if (dsnv[i].tknv == tknv) {
            return i
        }
    }
    return viTri;
}

function xuatThongTinNguoiDung(nv) {
    document.getElementById("tknv").value = nv.tknv;
    document.getElementById("name").value = nv.name;
    document.getElementById("email").value = nv.email;
    document.getElementById("password").value = nv.password;
    document.getElementById("datepicker").value = nv.datepicker;
    document.getElementById("luongCB").value = nv.luongCB;
    document.getElementById("chucvu").value = nv.chucvu;
    document.getElementById("gioLam").value = nv.giolam;
}