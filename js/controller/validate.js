function isNull(input, id) {
    if (input == "") {
        document.getElementById(id).innerHTML = `Không được để trống`
        document.getElementById(id).style.display = "block"
        return false
    } else {
        document.getElementById(id).style.display = "none"
        return true
    }
}

function kiemTraSoKyTu(input, id, min, max) {
    if (input.length < min || input.length > max) {
        document.getElementById(id).innerHTML = `Số ký tự tối đa ${min} - ${max}`
        document.getElementById(id).style.display = "block"
        return false
    } else {
        document.getElementById(id).style.display = "none"
        return true
    }
}

function kiemTraSo(input, id) {
    var reg = /^\d+$/;
    if (reg.test(input)) {
        document.getElementById(id).style.display = "none"
        return true
    } else {
        document.getElementById(id).innerHTML = `Ký tự nhập vào phải là ký số`
        document.getElementById(id).style.display = "block"
        return false
    }
}

function kiemTraChu(input, id) {
    var reg = /^[A-Za-z\s]*$/;
    if (reg.test(input)) {
        document.getElementById(id).style.display = "none"
        return true
    } else {
        document.getElementById(id).innerHTML = `Ký tự nhập vào phải là chữ`
        document.getElementById(id).style.display = "block"
        return false
    }
}

function kiemTraEmail(input, id) {
    var reg =
        /^(([^<>()[\]\.,;:\s@\"]+(\.[^<>()[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i;
    if (reg.test(input)) {
        document.getElementById(id).style.display = "none"
        return true
    } else {
        document.getElementById(id).innerHTML = `Nhập đúng định dạng email`
        document.getElementById(id).style.display = "block"
        return false
    }
}

function kiemTraPass(input, id) {
    var reg = /^(?=.*[0-9])(?=.*[!@#$%^&*])(?=.*[A-Z])[a-zA-Z0-9!@#$%^&*]{6,10}$/
    if (reg.test(input)) {
        document.getElementById(id).style.display = "none"
        return true
    } else {
        document.getElementById(id).innerHTML = `Mật Khẩu từ 6-10 ký tự (chứa ít nhất 1 ký tự số, 1 ký tự in hoa, 1 ký tự đặc biệt)`
        document.getElementById(id).style.display = "block"
        return false
    }
}

function kiemTraNgay(input, id) {
    var reg = /^((0?[1-9]|1[012])[- /.](0?[1-9]|[12][0-9]|3[01])[- /.](19|20)?[0-9]{2})*$/
    if (reg.test(input)) {
        document.getElementById(id).style.display = "none"
        return true
    } else {
        document.getElementById(id).innerHTML = `Định dạng mm/dd/yyyy`
        document.getElementById(id).style.display = "block"
        return false
    }
}

function kiemTraLuongCB(input, id) {
    if (input * 1 < 1000000 || input * 1 > 20000000) {
        document.getElementById(id).innerHTML = `Lương cơ bản 1 000 000 - 20 000 000`
        document.getElementById(id).style.display = "block"
        return false
    } else {
        document.getElementById(id).style.display = "none"
        return true
    }
}

function kiemTraChucVu(input, id) {
    if (input == "Sếp" || input == "Trưởng phòng" || input == "Nhân viên") {
        document.getElementById(id).style.display = "none"
        return true
    } else {
        document.getElementById(id).innerHTML = `Chọn chức vụ hợp lệ (Sếp, Trưởng Phòng, Nhân Viên)`
        document.getElementById(id).style.display = "block"
        return false
    }
}

function kiemTraSoGioLam(input, id){
    if (input * 1 < 80 || input * 1 > 200) {
        document.getElementById(id).innerHTML = `Số giờ làm trong tháng 80 - 200 giờ`
        document.getElementById(id).style.display = "block"
        return false
    } else {
        document.getElementById(id).style.display = "none"
        return true
    }
}